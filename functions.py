import os
import numpy as np
import pandas as pd 
import matplotlib.pyplot as plt
import seaborn as sns
import statsmodels.formula.api as smf
# import graphviz

from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor, GradientBoostingRegressor
from sklearn.model_selection import GridSearchCV
from sklearn import metrics
from sklearn.metrics import r2_score
from sklearn.tree import plot_tree
from sklearn.tree import export_graphviz
from sklearn.model_selection import cross_val_score
# from sklearn.neural_network import MLPRegressor
# from sklearn.preprocessing import LabelEncoder
# from sklearn.preprocessing import MinMaxScaler
from sklearn.inspection import PartialDependenceDisplay
# from sklearn.preprocessing import PolynomialFeatures, SplineTransformer
from sklearn.inspection import permutation_importance
from sklearn.linear_model import Ridge

from sklearn.tree import plot_tree
from sklearn.tree import DecisionTreeRegressor
from time import time

from ucimlrepo import fetch_ucirepo 


# 1. Random forest results
def ML_results(model, X, Y, X_train, Y_train, X_test, Y_test, model_name):
    # Y = df[[*X_col,'Concrete compressive strength_Mean']].dropna()['Concrete compressive strength_Mean']
    # X = df[[*X_col,'Concrete compressive strength_Mean']].dropna()[X_col].dropna()
    # X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=0)
    model.fit(X_train, Y_train)
    fit1 = model.fit(X_train, Y_train)
    y_pred_train = fit1.predict(X_train)
    y_pred_test = fit1.predict(X_test)
    y_pred_total = fit1.predict(X)

    # df[f'Concrete compressive strength_pred_{model_name}'] = y_pred_total
    

    print('********* RMSE Results *********')
    print('RMSE (test):', round(np.sqrt(metrics.mean_squared_error(Y_test, y_pred_test)), 4))
    print('RMSE (train):', round(np.sqrt(metrics.mean_squared_error(Y_train, y_pred_train)), 4))
    print('RMSE (total):', round(np.sqrt(metrics.mean_squared_error(Y, y_pred_total)), 4))
    print('*********************************')
    print('********* R Squared Results *****')
    print('R Squared (test):', round(r2_score(Y_test, y_pred_test), 4))
    print('R Squared (train):', round(r2_score(Y_train, y_pred_train), 4))
    print('R Squared (total):', round(r2_score(Y, y_pred_total), 4))
    print('*********************************')
    print('*********************************')

    plt.figure(figsize=(5,5))
    plt.scatter(Y, y_pred_total, c = 'b', alpha = 0.5, marker = '.', label = 'Real vs. Predicted')
    plt.plot(Y, Y, c = 'r', label = 'Perfect fit')
    plt.text(45, 13,f'R Squared (total): {round(r2_score(Y, y_pred_total), 4)}')
    plt.title(f'{model_name}')
    plt.xlabel('Concrete compressive strength_Mean: True value')
    plt.ylabel('Concrete compressive strength_Mean: Predicted value')
    plt.grid(color = '#D3D3D3', linestyle = 'solid')
    plt.legend(loc = 'lower right')
    plt.savefig(f'{model_name}_RMSE_Plot.jpg', dpi=600, bbox_inches="tight",
            pad_inches=0.3, transparent=True)
    plt.show()

# 2. Feature importance plot
def plot_feature_importance(X, model, model_name, figsize=(16,8)):
    # %matplotlib inline
    # Y = df[[*X_col,'Eff_Mean']].dropna()['Eff_Mean']
    # X = df[[*X_col,'Eff_Mean']].dropna()[X_col].dropna()
    
    names = X.columns
    importance = model.best_estimator_.feature_importances_
    #Create arrays from feature importance and feature names
    feature_importance = np.array(importance)
    feature_names = np.array(names)

    #Create a DataFrame using a Dictionary
    data={'feature_names':feature_names,'feature_importance':feature_importance}
    fi_df = pd.DataFrame(data)

    #Sort the DataFrame in order decreasing feature importance
    fi_df.sort_values(by=['feature_importance'], ascending=False,inplace=True)

    #Define size of bar plot
    plt.figure(figsize=figsize)
    #Plot Searborn bar chart
    sns.barplot(x=fi_df['feature_importance'], y=fi_df['feature_names'])
    #Add chart labels
    plt.title(model_name + ' '+'FEATURE IMPORTANCE')
    plt.xlabel('FEATURE IMPORTANCE')
    plt.ylabel('FEATURE NAMES')
    font1 = {'family': 'fantasy', 'color': 'blue', 'size': 20}
    font2 = {'family': 'serif', 'color': 'darkred', 'size': 10}
    font3 = {'family': 'cursive', 'color': 'green', 'size': 20}
    plt.ylabel('Features', fontdict=font2)
    plt.savefig(f'{model_name}_Feature_Importance.jpg', dpi=600, bbox_inches="tight",
            pad_inches=0.3, transparent=True)
    plt.show()

# 3. Permutation plots
def plot_permutation_feature_importance(X, Y, model, model_name, n_repeats=10, figsize=(16, 8)):
    # %matplotlib inline
    # Y = df[[*X_col, 'Eff_Mean']].dropna()['Eff_Mean']
    # X = df[[*X_col, 'Eff_Mean']].dropna()[X_col].dropna()
    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=0)

    result = permutation_importance(
        model, X_test, Y_test, n_repeats=n_repeats, random_state=42, n_jobs=2
    )
    sorted_idx = result.importances_mean.argsort()

    # Define size of bar plot
    plt.figure(figsize=figsize)
    plt.subplot(1, 2, 2)

    boxes = plt.boxplot(
        result.importances[sorted_idx].T,
        vert=False,
        labels=X_test.columns[sorted_idx],
        patch_artist=True,  # Enable coloring of boxes
    )

    # Define a list of distinct colors
    distinct_colors = ['red', 'blue', 'green', 'orange', 'purple', 'pink', 'cyan', 'yellow', 'brown', 'gray']

    # Assign distinct colors to each box
    for i, box in enumerate(boxes['boxes']):
        box.set_facecolor(distinct_colors[i % len(distinct_colors)])
        box.set_linewidth(2)  # Set border width of boxes

    font1 = {'family': 'fantasy', 'color': 'blue', 'size': 20}
    font2 = {'family': 'serif', 'color': 'darkred', 'size': 15}
    font3 = {'family': 'cursive', 'color': 'green', 'size': 20}
    plt.ylabel('Features', fontdict=font2)
    plt.title("Permutation Importance (test set)")
    plt.tight_layout()
    plt.savefig(f'{model_name}_permutation_plot.jpg', dpi=600, bbox_inches="tight",
            pad_inches=0.3, transparent=True)
    plt.show()


# 4. Partial dependency
def partial_dependency_func(X, Y, model, model_name, feature_list, ncols, nrows, figsize=(16, 30), fontsize=16):
    """ 
    N: Number of features
    model: model 
    """
        
    common_params = {
        "subsample": 50,
        "n_jobs": 2,
        "grid_resolution": 20,
        "random_state": 0,
    }

    print("Computing partial dependence plots...")
    features_info = {
        # features of interest
        "features": [*feature_list],
        # type of partial dependence plot
        "kind": "average",
        # information regarding categorical features
        # "categorical_features": categorical_features,
    }
    # tic = time()
    _, ax = plt.subplots(ncols=ncols, nrows=nrows, figsize=figsize, constrained_layout=True)
    display = PartialDependenceDisplay.from_estimator(
        model,
        X,
        **features_info,
        ax=ax,
        **common_params,
    )
    _ = display.figure_.suptitle(
        (
            "Partial dependence of Y\n"
            f"Based on {model_name}"
        ),
        fontsize=fontsize,
    )
    plt.savefig(f'{model_name}_PDP_plot.jpg', dpi=600)
    # print(f"done in {time() - tic:.3f}s")