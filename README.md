# Iman Khazrak

## Analysis of Concrete Compressive Strength Overview

This project aims to predict the compressive strength of concrete, a key measure in civil engineering, using the concrete_compressive_strength dataset. I implement three machine learning models: `Decision Tree`, `Random Forest`, and `Gradient Boosting`, to analyze the dataset comprising 8 features influencing concrete's strength.  

### Dataset

The concrete_compressive_strength dataset features are used to predict the material's compressive strength, focusing on quality and durability aspects. The dataset includes 8 predictive attributes.  

### Feature Selection

To identify the most influential features for predicting concrete's compressive strength, I employed several techniques:  

- **Feature Importance Plots**: Used to rank the features based on their importance in improving the model's predictive accuracy.  
- **Permutation Importance**: Assessed the impact of shuffling each feature on the model's accuracy to determine its significance.  
- **Partial Dependency Plots**: Evaluated how changes in the values of each feature affect the predicted outcome, providing insights into the relationship between the features and the target variable.  
These methods helped in discerning the critical features that significantly influence the compressive strength of concrete, guiding the model development process for better accuracy and interpretability.  

### Hyperparameters

**Decision Tree Hyperparameters**
```
parameters = {
    'max_features': [None, 'sqrt', 'log2'],
    'ccp_alpha': np.linspace(0, 1, 11)
}
```
**Random Forest and Gradient Boosting Hyperparameters**  

```
parameters = {
    'n_estimators': [20, 50, 100],
    'max_features': [None, 'sqrt', 'log2'],
    'ccp_alpha': np.linspace(0, 1, 11)
}
```

### Models and Results

#### Decision Tree
- **Results Summary**: The Decision Tree model explored various configurations, with the best performance achieved using no limit on the maximum features. The analysis highlighted the importance of the 'Age' feature as the most significant predictor of compressive strength.  

![alt text](Best_Decision_Tree_plot.png)

- **Best Parameters**: `{'ccp_alpha': 0.0, 'max_features': None}`  
- **Performance**:  

![alt text](image.png) 

<img src="Tree_cv5_RMSE_Plot.jpg" alt="alt text" width="600" height="400"/>

- **Feature Importances**: Age (0.3355), Cement (0.2894), Water (0.1169)  

<img src="Tree_cv5_Feature_Importance.jpg" alt="alt text" width="600" height="400"/>  

<img src="Tree_cv5_permutation_plot.jpg" alt="alt text" width="600" height="400"/>  

<img src="Tree_cv5_PDP_plot.jpg" alt="alt text" width="600" height="400"/>  



#### Random Forest

- **Results Summary**: The Random Forest model showed improved predictive accuracy over the Decision Tree, with the best settings using 'log2' for the maximum features and 100 estimators.  
- **Best Parameters**: `{'ccp_alpha': 0.0, 'max_features': 'log2', 'n_estimators': 100}`  
- **Performance**:  

![alt text](image-1.png)

<img src="RD_cv5_RMSE_Plot.jpg" alt="alt text" width="600

- **Feature Importances**: Age, Cement, Water   

<img src="RD_cv5_Feature_Importance.jpg" alt="alt text" width="600" height="400"/>  
<img src="RD_cv5_permutation_plot.jpg" alt="alt text" width="600" height="400"/>  
<img src="RD_cv5_PDP_plot.jpg" alt="alt text" width="600" height="400"/>  



#### Gradient Boosting

- **Results Summary**: Gradient Boosting further refined the predictions, optimizing for 'log2' maximum features and 100 estimators, showing strong performance in both training and test datasets.  
- **Best Parameters**: `{'ccp_alpha': 0.0, 'max_features': 'log2', 'n_estimators': 100}`  
- **Performance**:  

![alt text](image-2.png)

<img src="GradientBoosting_cv5_RMSE_Plot.jpg" alt="alt text" width="600" height="400"/>

- **Feature Importances**: Age, Cement, Water   

<img src="GradientBoosting_cv5_Feature_Importance.jpg" alt="alt text" width="600" height="400"/>  
<img src="GradientBoosting_cv5_permutation_plot.jpg" alt="alt text" width="600" height="400"/>  
<img src="GradientBoosting_cv5_PDP_plot.jpg" alt="alt text" width="600" height="400"/>  


### Best Model:

The best model is the Random Forest Model, which has the highest R-squared value and the lowest RMSE. Additionally, the RMSE results for both the test and training sets show that the difference between them is minimal, indicating that the model is not overfitted.  
